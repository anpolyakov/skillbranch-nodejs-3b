import express from 'express';
import cors from 'cors';
import _ from 'lodash';
import bodyParser from 'body-parser';
import fetch from 'isomorphic-fetch';

const app = express();
app.use(bodyParser.json());
app.use(cors());

const petUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json';

let fullPetUser = {};
fetch(petUrl)
  .then(async (res) => {
    fullPetUser = await res.json();
  })
  .catch(err => {
    console.log('Что-то пошло не так:', err);
  });

function notFound(res) {
  return res.status(404).send('Not Found');
}

function usersWithPet(rawUsers, petType) {
  return _.filter(rawUsers, function hasPet(user) {
    const pets = _.filter(fullPetUser.pets, { 'userId': user.id, 'type': petType });
    return pets.length; 
  });
}

function petsByType(rawPets, type) {
  return _.filter(rawPets, { 'type': type });
}
function petsAgeGT(rawPets, age) {
  return _.filter(rawPets, function filterGT(pet) { return pet.age > age; });
}

function petsAgeLT(rawPets, age) {
  return _.filter(rawPets, function filterLT(pet) { return pet.age < age; });
}
function populateUsers(rawUsers) {
  rawUsers.forEach((user) => {
    user['pets'] = _.filter(fullPetUser.pets, { 'userId': user.id });
  });  
  return rawUsers;
}

function populatePets(rawPets) {
  rawPets.forEach((pet) => {
    pet['user'] = _.filter(fullPetUser.users, { 'id': pet.userId })[0];
  });
  return rawPets;
}

app.get('/task3B/', (req, res) => {
  res.send(fullPetUser);
});

app.get('/task3B/users(/populate)?', (req, res) => {
  let selectedUsers = _.cloneDeep(fullPetUser.users);
  if (req.query.havePet != undefined) {
    selectedUsers = usersWithPet(selectedUsers, req.query.havePet);
  }
  if (req.path.indexOf('/populate') > 0) {
    res.send(JSON.stringify(populateUsers(selectedUsers)));
  } else {
    res.send(selectedUsers);
  }
});

app.get('/task3B/users/[-0-9a-zA-Z]*(/populate)?', (req, res) => {
  const pth = req.path.split('/');
  const userid = pth[3];
  const user = _.filter(fullPetUser.users, (usr) => { return (usr.id == userid || usr.username == userid); })[0];
  if (user) {
    if (req.path.indexOf('/populate') > 0) {
      let resultUser = _.cloneDeep(user);
      res.send(populateUsers([resultUser])[0]);
    } else {
      res.send(user);
    }
  } else {
    notFound(res);
  }
});

app.get('/task3B/pets(/populate)?', (req, res) => {
  let selectedPets = _.cloneDeep(fullPetUser.pets);
  if (req.query.type != undefined) {
    selectedPets = petsByType(selectedPets, req.query.type); 
  }
  if (req.query.age_gt != undefined) {
    selectedPets = petsAgeGT(selectedPets, req.query.age_gt);
  }
  if (req.query.age_lt != undefined) {
    selectedPets = petsAgeLT(selectedPets, req.query.age_lt);    
  }
  if (req.path.indexOf('/populate') > 0) {
    res.send(populatePets(selectedPets));
  } else {
    res.send(selectedPets);
  }
});

app.get('/task3B/pets/[-0-9]*(/populate)?', (req, res) => {
  const pth = req.path.split('/');

  const petid = +(pth[3]);
  const pet = _.filter(fullPetUser.pets, { 'id': petid })[0];
  if (pet) {
    if (req.path.indexOf('/populate') > 0) {
      let resultPet = _.cloneDeep(pet);
      res.send(populatePets([resultPet])[0]);
    } else {
      res.send(pet);
    }
  } else {
    notFound(res);
  }
});


app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
